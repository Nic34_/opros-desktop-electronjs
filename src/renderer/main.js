import Vue from 'vue'
import axios from 'axios'

import App from '../../core/App'
import router from '../../core/router'
import store from '../../core/store'
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueNestable from 'vue-nestable'

Vue.use(CKEditor);
Vue.use(VueNestable);

import {ipcRenderer} from 'electron';
const {remote} = require('electron')
const {Menu, MenuItem} = remote

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    components: {App},
    router,
    store,
    template: '<App/>',
    created() {
        ipcRenderer.on('navigate', (event, page) => {
            this.$router.push(page)
        });
        const menu = new Menu()
        menu.append(new MenuItem({ role: 'copy', label: 'Копировать' }))
        menu.append(new MenuItem({ role: 'paste', label: 'Вставить' }))
        menu.append(new MenuItem({ role: 'cut', label: 'Вырезать' }))
        window.addEventListener('contextmenu', (e) => {
            e.preventDefault()
            menu.popup(remote.getCurrentWindow())
        }, false)


    }
}).$mount('#app')
