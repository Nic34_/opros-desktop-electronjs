import Vue from 'vue'
import Vuex from 'vuex'
import 'whatwg-fetch'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        items: [],
        itemsRoot: [],
        itemsTemplates: [],
        itemsBeckaps: [],
        ids_remove: [],
        current: [],
        isLoad: true,
        isAuth: false,
    },
    getters: {
        beckapsForOpros: state => id => state.itemsBeckaps.filter(item => item.id)
    },
    mutations: {
        auth(state, pass) {
            if (pass == 'asdfgh') state.isAuth = true;
            if (window.localStorage) localStorage.isAuth = pass;
        },
        set_is_load(state, flag) {
            state.isLoad = flag;
        },
        update(state, item = {id: null, name: ''}) {
            let index = state.items.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.items[index] = item;
            else
                state.items.push(item)
        },
        'update-root'(state, item = {id: null, name: ''}) {
            let index = state.itemsRoot.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.itemsRoot[index] = item;
            else
                state.itemsRoot.push(item)
        },
        'update-beckaps'(state, item = {id: null, name: ''}) {
            let index = state.itemsBeckaps.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.itemsBeckaps[index] = item;
            else
                state.itemsBeckaps.push(item)
        },
        'update-template'(state, item = {id: null, name: ''}) {
            let index = state.itemsTemplates.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.itemsTemplates[index] = item;
            else
                state.itemsTemplates.push(item)
        },
        remove(state, id) {
            let index = state.items.findIndex(item => item.id == id)
            state.items.splice(index, 1);
        },
        clear(state) {
            state.items.splice(0);
            state.itemsRoot.splice(0);
        },
        'clear-list'(state) {
            state.itemsRoot.splice(0);
        },
        'cleare-remove'(state) {
            state.ids_remove.splice(0);
        },
        'clear-templates'(state) {
            state.itemsTemplates.splice(0);
        },
        'clear-beckaps'(state) {
            state.itemsBeckaps.splice(0);
        },
        append_remove(state, id) {
            async function rec(item) {
                remove.push(item.id)
                if (item.children) {
                    for (let child of item.children)
                        rec(child)
                }
            }

            if (!(id + '').includes('.')) {
                state.ids_remove.push(id);
                let subtree = state.items.find(item => item.id == id);
                rec(subtree);
            }
        },
        restoreBeckap(state, item) {
            console.log({item})
            let tree = JSON.parse(item.tree);
            state.items.splice(0);

            async function rec(item) {
                state.items.push(item)
                if (item.children) {
                    for (let child of item.children)
                        rec(child)
                }
            }

            rec(tree);
        },
        'update-sort-opros'(state, items) {
            state.itemsRoot.splice(0);
            for(let index in items) {
                state.itemsRoot.push({...items[index], sort: Number(index)+1});
            }
        },
    },
    actions: {
        async load({commit, dispatch}, id) {
            commit('clear');
            await fetch(`http://opros-server.j693917.myjino.ru/opros/index/${id}`)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update', item)
                })
            commit('set_is_load', false)
        },
        async 'load-list-opros'({commit, dispatch}) {
            commit('clear-list');
            await fetch(`http://opros-server.j693917.myjino.ru/opros/index`,)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update-root', item)
                })
            commit('set_is_load', false)
        },
        async 'load-list-templates'({commit, dispatch}) {
            commit('clear-templates');
            await fetch(`http://opros-server.j693917.myjino.ru/template/index`,)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update-template', item)
                })
            commit('set_is_load', false)
        },
        async 'load-list-beckaps'({commit, dispatch}, id) {
            commit('clear-beckaps');
            await fetch(`http://opros-server.j693917.myjino.ru/beckaps/index/${id}`,)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update-beckaps', item)
                })
            commit('set_is_load', false)
        },
        async save({state, dispatch, commit}, tree) {
            let root = {name: tree.name, desc: tree.desc, idParent: null, sort: tree.sort};
            if (tree.id.indexOf('.') == -1) root.id = tree.id;

            let idRoot = await fetch(`http://opros-server.j693917.myjino.ru/opros/save`, {
                method: 'post',
                body: JSON.stringify(root)
            }).then(res => res.text());

            await fetch(`http://opros-server.j693917.myjino.ru/opros/save`, {
                method: 'post',
                body: JSON.stringify({...root, idRoot, idParent: null})
            });

            for (let id of state.ids_remove) {
                await fetch(`http://opros-server.j693917.myjino.ru/opros/remove/${id}`);
            }

            async function rec(item, idParent, idRoot) {
                return new Promise(async resolve => {
                    let toSave = {name: item.name, desc: item.desc, img: item.img, idParent, idRoot, to: item.to, VideoSmall: item.VideoSmall, VideoBig: item.VideoBig};
                    if (item.id && item.id.toString().indexOf('.') == -1) toSave.id = item.id;
                    let id = await fetch(`http://opros-server.j693917.myjino.ru/opros/save`, {
                        method: 'post',
                        body: JSON.stringify(toSave)
                    }).then(r => r.text())

                    if (item.children) {
                        for (let child of item.children)
                            await rec(child, id, idRoot)
                    }
                    resolve();
                })
            }

            if (tree.children) {
                for (let child of tree.children)
                    await rec(child, idRoot, idRoot)
            }

            commit('cleare-remove');
            await dispatch('load', idRoot);
            await dispatch('load-list-opros');
        },
        async create({dispatch, state, commit}) {
            let id = await fetch(`http://opros-server.j693917.myjino.ru/opros/create`).then(r => r.text())
            dispatch('load-list-opros');
            return id;
        },
        async 'create-by-template'({dispatch, state, commit}, template) {
            console.log({dispacth: 'create-by-template', template})

            let tree = JSON.parse(template.tree);

            let root = {name: tree.name, desc: tree.desc, idParent: null};

            let idRoot = await fetch(`http://opros-server.j693917.myjino.ru/opros/createRoot`, {
                method: 'post',
                body: JSON.stringify(root)
            }).then(r => r.text())

            async function rec(item, idParent) {
                delete item.extend;
                let children = item.children;
                delete item.children;
                delete item.id;
                delete item.idParent;

                let id = await fetch(`http://opros-server.j693917.myjino.ru/opros/createItem`, {
                    method: 'post',
                    body: JSON.stringify({...item, idParent, idRoot})
                }).then(r => r.text())

                if (children) {
                    for (let child of children)
                        await rec(child, id)
                }
            }

            if (tree.children) {
                for (let child of tree.children)
                    await rec(child, idRoot)
            }
            await dispatch('load', idRoot);
            return idRoot
        },
        async removeOpros({dispatch, state}, tree) {
            console.log({dispatch: 'removeOpros', tree})
            let remove = [];

            async function rec(item) {
                remove.push(item.id)
                if (item.children) {
                    for (let child of item.children)
                        rec(child)
                }
            }

            rec(tree);

            for (let id of remove)
                await fetch(`http://opros-server.j693917.myjino.ru/opros/remove/${id}`,)

            return dispatch('load-list-opros');
        },
        async saveBeckap({state}, {tree, idOpros}) {
            return await fetch(`http://opros-server.j693917.myjino.ru/beckaps/create`, {
                method: 'post',
                body: JSON.stringify({tree: JSON.stringify(tree), idOpros})
            })
        },
        async saveTemplate({state}, {tree, idOpros}) {
            return await fetch(`http://opros-server.j693917.myjino.ru/template/create`, {
                method: 'post',
                body: JSON.stringify({tree: JSON.stringify(tree), idOpros})
            })
        },
        async updateSort({state, commit}, items) {
            if (!items.length) return;
            commit('update-sort-opros', items);
            return await fetch(`http://opros-server.j693917.myjino.ru/opros/updateSort`, {
                method: 'post',
                body: JSON.stringify(state.itemsRoot)
            })
        }
    }
})
