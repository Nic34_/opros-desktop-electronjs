import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/admin/',
      name: 'ListOpros',
      component: () => import(/* webpackChunkName: "admin" */ './views/ListOpros.vue'),
    },
    {
      path: '/admin/Auth',
      name: 'Auth',
      component: () => import(/* webpackChunkName: "Auth" */ './views/Auth.vue')
    },
    {
      path: '/admin/templates',
      name: 'templates',
      component: () => import(/* webpackChunkName: "admin" */ './views/ListTemplates.vue')
    },
    {
      path: '/admin/:id/:idShow?',
      name: 'admin',
      component: () => import(/* webpackChunkName: "admin" */ './views/Admin.vue')
    },
    {
      path: '/:idRoot?/:id?',
      name: 'home',
      component: Home
    }
  ],
})
